﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OWiki.Core.Tests
{
    public static class Sample
    {
        public static string Get(string key)
        {
			return ConfigurationManager.AppSettings.Get(key);
        }
    }
}
