﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using OWiki.Core.Concrete;

namespace OWiki.Core.Tests
{
    [TestFixture]
    public class WebServiceFactoryTests
    {
        private IWebServiceBuilder serviceBuilder;

        private WebServiceFactory target;

        [SetUp]
        public void Setup()
        {
            serviceBuilder = Substitute.For<IWebServiceBuilder>();

            target = new WebServiceFactory(serviceBuilder);
        }

        [TearDown]
        public void Teardown()
        {
            target = null;
        }

        [Test]
        public void CreateWebServiceShouldNotReturnNull()
        {
            IWebServiceBuilder result = target.CreateWebService();

            Assert.That(result, Is.Not.Null);
        }
    }
}