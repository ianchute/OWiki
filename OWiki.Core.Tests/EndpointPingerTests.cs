﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using OWiki.Core.Concrete;

namespace OWiki.Core.Tests
{
    [TestFixture]
    public class EndpointPingerTests
    {
        private readonly string downloadStringResult = Sample.Get("downloadStringResult");
        private readonly string endpoint = Sample.Get("endpoint");

        private IWebClient client;

        private EndpointPinger target;

        [SetUp]
        public void Setup()
        {
            client = Substitute.For<IWebClient>();

            target = new EndpointPinger(client);
        }

        [Test]
        public void PingShouldReturnFalseWhenClientReturnsNull()
        {
            client.Download().Returns((string)null);

            bool result = target.Ping(endpoint);

            Assert.That(result, Is.False);
        }

        [Test]
        public void PingShouldReturnTrueWhenClientReturnsNonNull()
        {
            client.Download().Returns(downloadStringResult);

            bool result = target.Ping(endpoint);

            Assert.That(result, Is.True);
        }
    }
}
