﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using OWiki.Core.Concrete;

namespace OWiki.Core.Tests
{
    [TestFixture]
    public class WebServiceTests
    {
        private IWebClient client;

        private WebService target;

        [SetUp]
        public void Setup()
        {
            client = Substitute.For<IWebClient>();

            target = new WebService(client);
        }

        [TearDown]
        public void Teardown()
        {
            target = null;
        }

        [Test]
        public void GetDataShouldNotReturnNull()
        {
            client.QueryString.Returns(new NameValueCollection());

            IWebRequestBuilder result = target.Request();

            Assert.That(result, Is.Not.Null);
        }
    }
}
