﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using OWiki.Core.Concrete;
using OWiki.Core.Models;

namespace OWiki.Core.Tests
{
    [TestFixture]
    public class WebRequestBuilderTests
    {
        private readonly string webRequestParameterKey = Sample.Get("webRequestParameterKey");
        private readonly string webRequestParameterValue = Sample.Get("webRequestParameterValue");
        private readonly string downloadStringResult = Sample.Get("downloadStringResult");

        private IWebClient client;

        private WebRequestBuilder target;

        [SetUp]
        public void Setup()
        {
            client = Substitute.For<IWebClient>();
            client.Download().Returns(downloadStringResult);

            target = new WebRequestBuilder(client);
        }

        [TearDown]
        public void Teardown()
        {
            target = null;
        }

        [Test]
        public void WithParameterShouldNotReturnNull()
        {
            IWebRequestBuilder result = target.WithParameter(webRequestParameterKey, webRequestParameterValue);

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void ExecuteShouldNotReturnNull()
        {
            client.QueryString.Returns(new NameValueCollection());

            WebServiceResponse result = target.Execute();

            Assert.That(result, Is.Not.Null);
        }
    }
}
