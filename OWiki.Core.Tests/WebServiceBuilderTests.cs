﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using OWiki.Core.Concrete;
using OWiki.Core.Exceptions;

namespace OWiki.Core.Tests
{
    [TestFixture]
    public class WebServiceBuilderTests
    {
        private readonly string endpoint = Sample.Get("endpoint");
        private readonly string userAgent = Sample.Get("userAgent");
        private readonly string customHeaderKey = Sample.Get("customHeaderKey");
        private readonly string customHeaderValue = Sample.Get("customHeaderValue");

        private IWebClient client;

        private WebServiceBuilder target;

        [SetUp]
        public void Setup()
        {
            client = Substitute.For<IWebClient>();
            client.Headers.Returns(new WebHeaderCollection());

            target = new WebServiceBuilder(client);
        }

        [TearDown]
        public void Teardown()
        {
            target = null;
        }

        [Test]
        public void WithEndpointShouldNotReturnNull()
        {
            IWebServiceBuilder result = target.WithEndpoint(endpoint);

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void WithEndpointShouldChangeClientBaseAddressToEndpoint()
        {
            target.WithEndpoint(endpoint);

            Assert.That(client.BaseAddress, Is.EqualTo(endpoint));
        }

        [Test]
        public void WithUserAgentShouldNotReturnNull()
        {
            IWebServiceBuilder result = target.WithUserAgent(userAgent);

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void WithUserAgentShouldAddHeaderKeyToClientHeaderKeys()
        {
            target.WithUserAgent(userAgent);

            Assert.That(client.Headers.AllKeys, Contains.Item(Strings.USER_AGENT_KEY_STRING));
        }

        [Test]
        public void WithUserAgentShouldAddHeaderValueToUserAgentHeader()
        {
            target.WithUserAgent(userAgent);

            Assert.That(client.Headers[Strings.USER_AGENT_KEY_STRING], Is.EqualTo(userAgent));
        }

        [Test]
        public void WithCustomHeaderShouldNotReturnNull()
        {
            IWebServiceBuilder result = target.WithCustomHeader(customHeaderKey, customHeaderValue);

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void WithCustomHeaderShouldAddHeaderKeyToClientHeaderKeys()
        {
            target.WithCustomHeader(customHeaderKey, customHeaderValue);

            Assert.That(client.Headers.AllKeys, Contains.Item(customHeaderKey));
        }

        [Test]
        public void WithCustomHeaderShouldAddHeaderValueToClientHeaders()
        {
            target.WithCustomHeader(customHeaderKey, customHeaderValue);

            Assert.That(client.Headers[customHeaderKey], Is.EqualTo(customHeaderValue));
        }

        [Test]
        [ExpectedException(typeof(WebServiceBuilderException))]
        public void BuildShouldThrowAnExceptionWhenNoEndpointIsSpecified()
        {
            target.Build();
        }

        [Test]
        public void BuildShouldNotReturnNullWhenAnEndpointIsSpecified()
        {
            IWebService result = target
                .WithEndpoint(endpoint)
                .Build();

            Assert.That(result, Is.Not.Null);
        }
    }
}