﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OWiki.Core.Models;

namespace OWiki.Core
{
    public interface IWebRequestBuilder
    {
        IWebRequestBuilder WithParameter(string key, string value);

        WebServiceResponse Execute();
    }
}