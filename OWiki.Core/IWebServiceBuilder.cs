﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OWiki.Core
{
    public interface IWebServiceBuilder
    {
        IWebServiceBuilder WithEndpoint(string endpoint);

        IWebServiceBuilder WithUserAgent(string userAgent);

        IWebServiceBuilder WithCustomHeader(string key, string value);

        IWebService Build();
    }
}