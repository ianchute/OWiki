﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OWiki.Core.Exceptions
{
    public class WebRequestExecutionException : Exception
    {
        public WebRequestExecutionException()
        {
        }

        public WebRequestExecutionException(Exception e)
            : base(e.Message, e)
        {
        }
    }
}
