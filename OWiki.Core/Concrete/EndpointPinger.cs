﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace OWiki.Core.Concrete
{
    public class EndpointPinger : IEndpointPinger
    {
        private IWebClient client;

        public EndpointPinger(IWebClient client)
        {
            this.client = client;
        }

        public bool Ping(string endpoint)
        {
            client.BaseAddress = endpoint;
            try
            {
                if(client.Download() != null)
                    return true;
                return false;
            }
            catch
            {
                return false;
            }
        }
    }
}
