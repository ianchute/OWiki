﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace OWiki.Core.Concrete
{
    public class WebService : IWebService
    {
        private IWebClient client;

        public WebService(IWebClient client)
        {
            this.client = client;
        }

        public IWebRequestBuilder Request()
        {
            ResetWebClient();
            return new WebRequestBuilder(client);
        }

        private void ResetWebClient()
        {
            client.QueryString.Clear();
        }
    }
}
