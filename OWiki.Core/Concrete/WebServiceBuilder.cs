﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using OWiki.Core.Exceptions;

namespace OWiki.Core.Concrete
{
    public class WebServiceBuilder : IWebServiceBuilder
    {
        private IWebClient client;

        public WebServiceBuilder(IWebClient client)
        {
            this.client = client;
        }

        public IWebServiceBuilder WithEndpoint(string endpoint)
        {
            client.BaseAddress = endpoint;

            return this;
        }

        public IWebServiceBuilder WithUserAgent(string userAgent)
        {
            client.Headers.Add(Strings.USER_AGENT_KEY_STRING, userAgent);

            return this;
        }

        public IWebServiceBuilder WithCustomHeader(string key, string value)
        {
            client.Headers.Add(key, value);

            return this;
        }

        public IWebService Build()
        {
            if (string.IsNullOrWhiteSpace(client.BaseAddress))
                throw new WebServiceBuilderException();

            return new WebService(client);
        }
    }
}
