﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace OWiki.Core.Concrete
{
    public class WebServiceFactory : IWebServiceFactory
    {
        private IWebServiceBuilder builder;

        public WebServiceFactory(IWebServiceBuilder builder)
        {
            this.builder = builder;
        }

        public IWebServiceBuilder CreateWebService()
        {
            return builder;
        }
    }
}
