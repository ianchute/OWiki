﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using OWiki.Core.Enums;
using OWiki.Core.Exceptions;
using OWiki.Core.Models;

namespace OWiki.Core.Concrete
{
    public class WebRequestBuilder : IWebRequestBuilder
    {
        private IWebClient client;
        private IDictionary<string, string> parameters;

		static WebRequestBuilder()
		{
			ServicePointManager.CertificatePolicy = new NoCheckCertificatePolicy();
		}

        public WebRequestBuilder(IWebClient client)
        {
            this.client = client;
            this.parameters = new SortedDictionary<string, string>();
        }

        public IWebRequestBuilder WithParameter(string key, string value)
        {
            parameters.Add(key, value);
            return this;
        }

        public WebServiceResponse Execute()
        {
            foreach(var parameter in parameters)
            {
                client.QueryString.Add(parameter.Key, parameter.Value);
            }

            RequestForContentInQueryString();

            string response = string.Empty;
            DateTime start = DateTime.MinValue;
            DateTime received = DateTime.MinValue;
            TimeSpan duration = TimeSpan.Zero;

            start = DateTime.Now;

            try
            {
                response = client.Download();
            }
            catch(Exception e)
            {
                throw new WebRequestExecutionException(e);
            }

            received = DateTime.Now;
            duration = received.Subtract(start);

            return new WebServiceResponse()
            {
                Endpoint = client.BaseAddress,
                QueryParameters = client.QueryString.AllKeys
                    .ToDictionary(_ => _, _ => client.QueryString[_]),
                RequestStarted = start,
                ResponseReceived = received,
                ExecutionDuration = duration,
                Content = response
            };
        }

        private void RequestForContentInQueryString()
        {
            client.QueryString.Add("rvprop", "content");
        }
    }
}
