﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace OWiki.Core.Concrete
{
    public class WebClientWrapper : IWebClient
    {
        private WebClient client;

        public WebClientWrapper(WebClient client)
        {
            this.client = client;

            ApplyOptimizations();
        }

        public NameValueCollection QueryString
        {
            get
            {
                return client.QueryString;
            }
            set
            {
                client.QueryString = value;
            }
        }

        public string Download()
        {
            return client.DownloadString(string.Empty);
        }

        public string BaseAddress
        {
            get
            {
                return client.BaseAddress;
            }
            set
            {
                client.BaseAddress = value;
            }
        }

        public WebHeaderCollection Headers
        {
            get
            {
                return client.Headers;
            }
            set
            {
                client.Headers = value;
            }
        }

        private void ApplyOptimizations()
        {
            client.Proxy = null;
        }
    }
}
