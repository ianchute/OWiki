using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using OWiki.Core.Enums;
using OWiki.Core.Exceptions;
using OWiki.Core.Models;
using System.Security.Cryptography.X509Certificates;

namespace OWiki.Core.Concrete
{
	public class NoCheckCertificatePolicy : ICertificatePolicy
	{
		public bool CheckValidationResult (ServicePoint srvPoint, X509Certificate certificate, WebRequest request, int certificateProblem)
		{
			return true;
		}
	}
}
