﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OWiki.Core
{
    public interface IWebServiceFactory
    {
        IWebServiceBuilder CreateWebService();
    }
}