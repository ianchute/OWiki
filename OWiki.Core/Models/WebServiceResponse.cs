﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OWiki.Core.Models
{
    public class WebServiceResponse
    {
        public string Endpoint { get; set; }

        public string Content { get; set; }

        public DateTime RequestStarted { get; set; }

        public DateTime ResponseReceived { get; set; }

        public TimeSpan ExecutionDuration { get; set; }

        public Dictionary<string, string> QueryParameters { get; set; }
    }
}