﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace OWiki.Core
{
    public interface IWebClient
    {
        NameValueCollection QueryString { get; set; }

        string Download();

        string BaseAddress { get; set; }

        WebHeaderCollection Headers { get; set; }
    }
}
