﻿using System;
using System.Net;
using OWiki.Core.Concrete;
using SimpleInjector;

namespace OWiki.Core
{
    public class ServiceProvider : IServiceProvider
    {
        private Container container;
        public ServiceProvider()
        {
            this.container = new Container();

            container.Register<IWebClient, WebClientWrapper>();
            container.Register<IWebRequestBuilder, WebRequestBuilder>();
            container.Register<IWebService, WebService>();
            container.Register<IWebServiceBuilder, WebServiceBuilder>();
            container.Register<IWebServiceFactory, WebServiceFactory>();
            container.Register<IEndpointPinger, EndpointPinger>();
            container.Register<WebClient>();

            container.Verify();
        }

        public object GetService(Type serviceType)
        {
            try
            {
                object service = this.container.GetInstance(serviceType);
                return service;
            }
            catch
            {
                return null;
            }
        }
    }
}