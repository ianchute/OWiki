﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OWiki.Concrete;
using OWiki.Enums;
using OWiki.Models;

namespace OWiki
{
    public interface IWikiQueryBuilder
    {
        IWikiQueryBuilder Format(string format);

        IWikiQueryBuilder Format(WikiFormat format);

        IWikiQueryBuilder Action(string action);

        IWikiQueryBuilder Action(WikiAction action);

        IWikiQueryBuilder Titles(IEnumerable<string> titles);

        IWikiQueryBuilder Property(string property);

        IWikiQueryBuilder Property(WikiProp property);

        WikiResult Execute();
    }
}