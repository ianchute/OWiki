﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OWiki.Mappers
{
    public interface IMapper<T, U>
    {
            U Map(T obj);
    }
}