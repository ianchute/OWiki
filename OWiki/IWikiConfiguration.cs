﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OWiki
{
    public interface IWikiConfiguration
    {
        string Endpoint { get; }

        string BackupEndpoint { get; }

        string LanguageCode { get; }

        string UserAgent { get; }

        string TitleSeparator { get; }
    }
}