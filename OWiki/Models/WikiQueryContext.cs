﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OWiki.Models
{
    public class WikiQueryContext
    {
        public string Format { get; set; }
        
        public string Action { get; set; }

        public string[] Titles { get; set; }

        public string Property { get; set; }
    }
}
