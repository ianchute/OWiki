﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using OWiki.Concrete;
using OWiki.Core.Models;
using OWiki.Mappers;
using OWiki.Models;
using SimpleInjector;
using SimpleInjector.Advanced;

namespace OWiki
{
    public class ServiceProvider : IServiceProvider
    {
        IServiceProvider coreServiceProvider;

        Container container;

        public ServiceProvider()
        {
            this.coreServiceProvider = new Core.ServiceProvider();
            container = new Container();

            container.Register<Core.IWebServiceFactory>(() =>
                (Core.IWebServiceFactory)coreServiceProvider.GetService(typeof(Core.IWebServiceFactory)));
            container.Register<WikiQueryContext>();
            container.Register<IWikiQueryBuilder, WikiQueryBuilder>();
            container.Register<IWikiConfiguration, DefaultWikiConfiguration>();
            container.Register<IMapper<WebServiceResponse, WikiResult>, WsrToWrMapper>();

            container.Verify();
        }

        public object GetService(Type serviceType)
        {
            try
            {
                object result = container.GetInstance(serviceType);
                return result;
            }
            catch
            {
                return null;
            }
        }
    }
}