﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OWiki.Models
{
    public class DefaultWikiConfiguration : IWikiConfiguration
    {
        public string Endpoint 
        { 
            get 
            {
                return "https://en.wikipedia.org/w/api.php";
            } 
        }

        public string BackupEndpoint
        {
            get
            {
                return "https://en.wikipedia.org/w/api.php";
            }
        }

        public string LanguageCode
        {
            get
            {
                return "en";
            }
        }

        public string UserAgent
        {
            get 
            { 
                return "OWiki/1.0 (https://github.com/ianchute/OWiki; ianchute@hotmail.com) OWikiCore/1.0"; 
            }
        }

        public string TitleSeparator
        {
            get { return "|"; }
        }
    }
}