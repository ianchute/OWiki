﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OWiki.Core.Models;
using OWiki.Enums;
using OWiki.Mappers;
using OWiki.Models;

namespace OWiki.Concrete
{
    public class WikiQueryBuilder : IWikiQueryBuilder
    {
        private readonly Core.IWebServiceFactory serviceFactory;
        private readonly WikiQueryContext context;
        private readonly IWikiConfiguration configuration;
        private readonly IMapper<WebServiceResponse, WikiResult> mapper;

        public WikiQueryBuilder(
            Core.IWebServiceFactory serviceFactory, 
            WikiQueryContext context, 
            IWikiConfiguration configuration,
            IMapper<WebServiceResponse, WikiResult> mapper)
        {
            this.serviceFactory = serviceFactory;
            this.context = context;
            this.configuration = configuration;
            this.mapper = mapper;
        }

        public IWikiQueryBuilder Format(string format)
        {
            context.Format = format;
            return this;
        }

        public IWikiQueryBuilder Action(string action)
        {
            context.Action = action;
            return this;
        }

        public IWikiQueryBuilder Property(string property)
        {
            context.Property = property;
            return this;
        }

        public IWikiQueryBuilder Format(WikiFormat format)
        {
            return Format(format.ToString().ToLowerInvariant());
        }

        public IWikiQueryBuilder Action(WikiAction action)
        {
            return Action(action.ToString().ToLowerInvariant());
        }

        public IWikiQueryBuilder Property(WikiProp property)
        {
            return Property(property.ToString().ToLowerInvariant());
        }

        public IWikiQueryBuilder Titles(IEnumerable<string> titles)
        {
            context.Titles = titles.ToArray();
            return this;
        }

        public WikiResult Execute()
        {
            Core.IWebService service = serviceFactory.CreateWebService()
                .WithEndpoint(configuration.Endpoint)
                .WithUserAgent(configuration.UserAgent)
                .Build();

            string titles = context.Titles != null 
                ? string.Join(configuration.TitleSeparator, context.Titles) 
                : string.Empty;
            Core.Models.WebServiceResponse response =
                service.Request()
                .WithParameter("action", context.Action)
                .WithParameter("format", context.Format)
                .WithParameter("prop", context.Property)
                .WithParameter("titles", titles)
                .Execute();

            return mapper.Map(response);
        }
    }
}
