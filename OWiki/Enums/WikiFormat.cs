﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OWiki.Enums
{
    public enum WikiFormat
    {
        dbg, 
        dbgfm, 
        dump, 
        dumpfm, 
        json, 
        jsonfm, 
        none, 
        php, 
        phpfm, 
        rawfm, 
        txt, 
        txtfm, 
        wddx, 
        wddxfm, 
        xml, 
        xmlfm, 
        yaml, 
        yamlfm
    }
}
