﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OWiki.Enums
{
    public enum WikiAction
    {
        abusefiltercheckmatch, 
        abusefilterchecksyntax, 
        abusefilterevalexpression, 
        abusefilterunblockautopromote, 
        addstudents, 
        antispoof, 
        block, 
        bouncehandler, 
        centralauthtoken, 
        centralnoticebannerchoicedata, 
        centralnoticequerycampaign, 
        checktoken, 
        cirrus_config_dump, 
        cirrus_mapping_dump, 
        cirrus_settings_dump, 
        clearhasmsg,
        compare,
        createaccount,
        delete, 
        deleteeducation, 
        deleteglobalaccount, 
        echomarkread, 
        echomarkseen, 
        edit, 
        editlist, 
        editmassmessagelist, 
        emailuser, 
        enlist, 
        expandtemplates, 
        fancycaptchareload, 
        featuredfeed, 
        feedcontributions, 
        feedrecentchanges, 
        feedwatchlist, 
        filerevert, 
        flagconfig, 
        flow, 
        flow_parsoid_utils, 
        flowthank,
        globalblock, 
        globaluserrights, 
        help, 
        imagerotate, 
        import, 
        jsonconfig, 
        languagesearch, 
        liststudents, 
        login, 
        logout, 
        managetags, 
        massmessage, 
        mobileview, 
        move, 
        opensearch, 
        options, 
        pagetriageaction, 
        pagetriagelist,
        pagetriagestats, 
        pagetriagetagging, 
        pagetriagetemplate, 
        paraminfo, 
        parse, 
        patrol, 
        protect, 
        purge, 
        query, 
        refresheducation, 
        review, 
        reviewactivity, 
        revisiondelete, 
        rollback, 
        rsd, 
        scribunto_console, 
        setglobalaccountstatus, 
        setnotificationtimestamp, 
        sitematrix, 
        spamblacklist, 
        stabilize, 
        stashedit, 
        tag, 
        templatedata, 
        thank, 
        titleblacklist, 
        tokens, 
        transcodereset, 
        ulslocalization,
        unblock, 
        undelete, 
        upload, 
        userdailycontribs, 
        userrights, 
        visualeditor, 
        visualeditoredit, 
        watch, 
        wikilove, 
        zeroconfig
    }
}
