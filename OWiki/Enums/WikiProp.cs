﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OWiki.Enums
{
    public enum WikiProp
    {
        categories,
        categoryinfo,
        contributors,
        duplicatefiles,
        extlinks,
        fileusage,
        imageinfo,
        images,
        info,
        iwlinks,
        langlinks,
        links,
        linkshere,
        pageprops,
        redirects,
        revisions,
        stashimageinfo,
        templates,
        transcludedin
    }
}