﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using OWiki.Enums;
using OWiki.Models;

namespace OWiki.Tests
{
    [TestFixture]
    public class E2ETests
    {
        private IWikiQueryBuilder queryBuilder;

        [SetUp]
        public void Setup()
        {
            ServiceProvider provider = new ServiceProvider();
            queryBuilder = (IWikiQueryBuilder)provider.GetService(typeof(IWikiQueryBuilder));
        }

        [Test]
        public void z_WikiQueryBuilderHappyPath()
        {
            queryBuilder
                .Action(WikiAction.query)
                .Format(WikiFormat.json)
                .Property(WikiProp.revisions)
                .Titles(new string[] { "Wikipedia" })
                .Execute();

            Assert.Pass();
        }
    }
}
