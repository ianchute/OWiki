﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NSubstitute;
using NUnit.Framework;
using OWiki.Concrete;
using OWiki.Core.Models;
using OWiki.Enums;
using OWiki.Mappers;
using OWiki.Models;

namespace OWiki.Tests
{
    [TestFixture]
    public class WikiQueryBuilderTests
    {
        private readonly string FORMAT = Sample.Get("formatString");
        private readonly string ACTION = Sample.Get("actionString");
        private readonly string TITLE1 = Sample.Get("titleString1");
        private readonly string TITLE2 = Sample.Get("titleString2");
        private readonly string TITLE3 = Sample.Get("titleString3");
        private readonly string PROPERTY = Sample.Get("propertyString");

        private string[] titles;
        private Core.IWebServiceFactory factory;
        private WikiQueryContext context;
        private IWikiConfiguration configuration;
        private IMapper<WebServiceResponse, WikiResult> mapper;

        private WikiQueryBuilder target;

        [SetUp]
        public void Setup()
        {
            factory = Substitute.For<Core.IWebServiceFactory>();
            context = Substitute.For<WikiQueryContext>();
            configuration = Substitute.For<IWikiConfiguration>();
            mapper = Substitute.For<IMapper<WebServiceResponse, WikiResult>>();
            titles = new string[3] { TITLE1, TITLE2, TITLE3 };

            target = new WikiQueryBuilder(factory, context, configuration, mapper);
        }

        [TearDown]
        public void Teardown()
        {
            factory = null;
            context = null;
            target = null;
        }

        [Test]
        public void FormatShouldModifyUnderlyingQueryContext()
        {
            target.Format(FORMAT);

            Assert.That(context.Format, Is.EqualTo(FORMAT));
        }

        [Test]
        public void ActionShouldModifyUnderlyingQueryContext()
        {
            target.Action(ACTION);

            Assert.That(context.Action, Is.EqualTo(ACTION));
        }

        [Test]
        public void TitlesShouldModifyUnderlyingQueryContext()
        {
            target.Titles(titles);

            Assert.That(context.Titles, Is.EqualTo(titles));
        }

        [Test]
        public void PropertyShouldModifyUnderlyingQueryContext()
        {
            target.Property(PROPERTY);

            Assert.That(context.Property, Is.EqualTo(PROPERTY));
        }

        [Test]
        public void FormatShouldNotReturnNull()
        {
            IWikiQueryBuilder result = target.Format(FORMAT);

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void ActionShouldNotReturnNull()
        {
            IWikiQueryBuilder result = target.Action(ACTION);

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void TitlesShouldNotReturnNull()
        {
            IWikiQueryBuilder result = target.Titles(titles);

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void PropertyShouldNotReturnNull()
        {
            IWikiQueryBuilder result = target.Property(PROPERTY);

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void ExecuteShouldNotReturnNullWhenNoConfigurationsAreSet()
        {
            WikiResult result = target.Execute();

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void ExecuteShouldNotReturnNullWhenAllConfigurationsAreSet()
        {
            target.Format(FORMAT);
            target.Action(ACTION);
            target.Titles(titles);
            target.Property(PROPERTY);

            WikiResult result = target.Execute();

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void FormatWithEnumArgShouldModifyUnderlyingQueryContext()
        {
            target.Format(WikiFormat.json);

            Assert.That(context.Format, Is.EqualTo(WikiFormat.json.ToString().ToLowerInvariant()));
        }

        [Test]
        public void ActionWithEnumArgShouldModifyUnderlyingQueryContext()
        {
            target.Action(WikiAction.query);

            Assert.That(context.Action, Is.EqualTo(WikiAction.query.ToString().ToLowerInvariant()));
        }

        [Test]
        public void PropertyWithEnumArgShouldModifyUnderlyingQueryContext()
        {
            target.Property(WikiProp.revisions);

            Assert.That(context.Property, Is.EqualTo(WikiProp.revisions.ToString().ToLowerInvariant()));
        }

        [Test]
        public void FormatWithEnumArgShouldNotReturnNull()
        {
            IWikiQueryBuilder result = target.Format(WikiFormat.json);

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void ActionWithEnumArgShouldNotReturnNull()
        {
            IWikiQueryBuilder result = target.Action(WikiAction.query);

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void PropertyWithEnumArgShouldNotReturnNull()
        {
            IWikiQueryBuilder result = target.Property(WikiProp.revisions);

            Assert.That(result, Is.Not.Null);
        }
    }
}
